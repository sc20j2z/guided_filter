clc; clear;

filename = 'bun_zipper';
amplitude = 0.005;


pcl = dlmread(filename);
noisy_pcl = pcl + amplitude * randn(size(pcl));

fileparts = strsplit(filename, '.');
dlmwrite( sprintf('%s_%s.xyz', fileparts{1}, 'noisy2'), noisy_pcl, ' ')