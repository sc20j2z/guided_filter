guided_filter Environment Configuration
QT 5.15.2 Third-party Libraries: eigen, pcl

Usage Instructions:
Download the guided_filter code and third-party library eigen from GitLab.
Copy all contents of eigen into the guided_filter/external/eigen folder.
Copy the shader folder into the build directory created by QT.
Open the guided_filter.pro file with QT and run.
After running guided_filter, the MainWindow interface is displayed.
Use the navigation bar buttons on the MainWindow interface to complete rendering and post-filter rendering, and obtain the CD value.
——————————————————————————————————————
MainWindow Interface Right Navigation Bar Introduction:

Files
Load noisy PCL: Click to load a noisy point cloud file (initial point cloud file) from your local computer.
Load groundtruth PCL: Click to load a groundtruth point cloud file from your local computer.

Rendering
Radius: Change the rendering radius of each point by pressing buttons or entering a value.
Toggle noisy PCL: Display the rendered point cloud image with noise.
Toggle filtered PCL: Display the rendered point cloud image after filtering.
Toggle groundtruth PCL: Display the groundtruth point cloud image.

Filter
knn: Choose to use the KNN method for filtering and change the KNN parameter by pressing buttons or entering a value.
range: Choose to use the radius method for filtering and change the radius parameter by pressing buttons or entering a value.
epsilon: Change the epsilon parameter by pressing buttons or entering a value.

Metrics
Chamfer Distance: Display the CD value for this rendering method.

GroupBox
Save filtered PCL: Save the filtered point cloud file to a custom local path.

——————————————————————————————————————
Rendering Steps:

Select the initial point cloud data and groundtruth data.
Choose the rendering radius size and window display rendering image type.
Select the filtering method and change the parameters.
Obtain the CD value.
Save the filtered point cloud file.
