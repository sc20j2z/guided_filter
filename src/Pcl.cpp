#include "Pcl.h"

Pcl::Pcl()
{
}

bool Pcl::isEmpty() const
{
    return m_points.rows() == 0;
}

void Pcl::generateRandomPoints(int num_points)
{
    m_points.setRandom(num_points, 3);

    // post-processing.
    this->findBBox();
}

void Pcl::readFile(const std::string &filename)
{
    std::ifstream fin(filename);

    if (!fin.is_open())
        throw std::runtime_error("cannot open" + filename);

    // read how many lines to reserve memory
    int number_of_lines = 0;
    std::string line;

    while (std::getline(fin, line))
            ++number_of_lines;

    // https://stackoverflow.com/a/7681612
    fin.clear();
    fin.seekg(0);

    m_points.resize(number_of_lines, 3);
    for(int i=0; i<number_of_lines; ++i)
        fin >> m_points(i, 0) >> m_points(i, 1) >> m_points(i, 2);

#if 0
    int n = 0;
    while(fin >> m_points(n, 0) >> m_points(n, 1) >> m_points(n, 2) >> m_intensities[n] >> m_colours(n, 0) >> m_colours(n, 1) >> m_colours(n, 2))
        ++n;
#endif

    std::cout << "read " << m_points.rows() << " from " << filename <<std::endl;
    // generate a debug file some noise
    //m_points(Eigen::placeholders::all, 1) = 0.02 * Eigen::Matrix<float, -1, 1>::Random(m_points.rows(), 1);
    //this->writeFile("../data/plane_noisy.xyz");

    this->findBBox();
}

void Pcl::writeFile(const std::string &filename)
{
    std::ofstream fout(filename);

    if (fout.is_open())
    {
        for(Eigen::Index i=0; i< m_points.rows(); ++i)
            fout << m_points(i, 0) << " " << m_points(i, 1) << " " << m_points(i, 2) << std::endl;
    }
}

void Pcl::setPoints(Eigen::Matrix<float, -1, 3, Eigen::RowMajor> &points)
{
    m_points = points;
}

void Pcl::setupVAO()
{
    // Initialize and bind the VAO that's going to capture all this vertex state
    if (!m_vao.isCreated()) m_vao.create();
    m_vao.bind();

    // vertex buffer.
    if (!m_pbo.isCreated())
    {
        m_pbo = QOpenGLBuffer(QOpenGLBuffer::VertexBuffer);
        m_pbo.create();
    }
    m_pbo.bind();
    m_pbo.setUsagePattern(QOpenGLBuffer::StaticDraw);
    m_pbo.allocate(m_points.data(), m_points.size() * sizeof(float));

    m_vao.release();
}

void Pcl::draw(QOpenGLShaderProgram *shader)
{
    if (!m_vao.isCreated())
        return;

    shader->setUniformValue("objectColor", m_color);

    m_vao.bind();

    // activate vertex attribute.
    m_pbo.bind();
    shader->setAttributeBuffer(0, GL_FLOAT, 0, 3);
    shader->enableAttributeArray(0);

    glDrawArrays(GL_POINTS, 0, m_points.rows());

    m_vao.release();
}

const Eigen::Matrix<float, -1, 3, Eigen::RowMajor> &Pcl::getPoints() const
{
    return m_points;
}

void Pcl::setColor(const QVector3D &color)
{
    m_color = color;
}

void Pcl::findBBox()
{
    m_bbox = Eigen::AlignedBox<float, 3>(m_points.colwise().minCoeff(), m_points.colwise().maxCoeff());
}

Eigen::AlignedBox<float, 3> Pcl::getBBox() const
{
    return m_bbox;
}
