#include "Pcl.h"
#include "kdtree.h"

int main(int argc, char *argv[])
{
    Pcl pcl;
    pcl.generateRandomPoints(1000);
    auto pts = pcl.getPoints();

    KdTree tree;
    tree.create(pts, 3);

    Eigen::Matrix<float, 1, 3> query_pt = Eigen::Matrix<float, 1, 3>::Random();

    /* test knn search */  
    int k = 5;
    std::vector<int> knn_nbrs = tree.knnSearch(query_pt, k);
    std::sort(knn_nbrs.begin(), knn_nbrs.end());

    Eigen::VectorXf distances(pts.rows());
    for(int i=0; i<pts.rows(); ++i)
        distances[i] = (query_pt - pts.row(i)).norm();

    Eigen::VectorXi order = Eigen::VectorXi::LinSpaced(pts.rows(), 0, pts.rows()-1);
    std::sort(order.begin(), order.end(), [&distances](int i, int j) {return distances[i] < distances[j];});

    std::vector<int> brutal_nbrs;
    for(int i=0; i<k; ++i)
        brutal_nbrs.push_back(order[i]);
    std::sort(brutal_nbrs.begin(), brutal_nbrs.end());


    std::cout << "---- test KNN search ----" << std::endl;
    for(int i=0; i<k; ++i)
        std::cout << "kdtree knn: " << knn_nbrs[i] << ", brutal force: " << brutal_nbrs[i] <<std::endl;

     /* test range search */
    float range = 0.3;
    std::vector<int> range_nbrs = tree.rangeSearch(query_pt, range);
    std::sort(range_nbrs.begin(), range_nbrs.end());

    std::vector<int> nbrs2;
    for(int i=0; i<pts.rows(); ++i)
    {
        if ((query_pt - pts.row(i)).norm() <= range)
            nbrs2.push_back(i);
    }

    std::cout << "---- test range search ----" << std::endl;
    for(int i=0; i<range_nbrs.size(); ++i)
        std::cout << "kdtree range: " << range_nbrs[i] << ", brutal force: " << nbrs2[i] << std::endl;
    return 0;
}
