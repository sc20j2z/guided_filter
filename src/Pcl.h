#ifndef PCL_H
#define PCL_H

#include <cmath>
#include <iostream>
#include <fstream>
#include <vector>

#include <QOpenGLFunctions>
#include <QOpenGLExtraFunctions> // OpenGL ES 3.0, 3.1 and 3.2
#include <QOpenGLVertexArrayObject>
#include <QOpenGLBuffer>
#include <QDebug>
#include <QOpenGLShaderProgram>

#include <Eigen/Dense>
#include <Eigen/Geometry>

#include "kdtree.h"

class Pcl
{
public:
    Pcl();

    bool isEmpty() const;

    void generateRandomPoints(int num_points);
    void readFile(const std::string & filename);
    void writeFile(const std::string & filename);

    void setupVAO();
    void draw(QOpenGLShaderProgram *shader);

    void setPoints(Eigen::Matrix<float, -1, 3, Eigen::RowMajor> & points);
    const Eigen::Matrix<float, -1, 3, Eigen::RowMajor> &getPoints() const;

    void setColor(const QVector3D & color);

    // fit PCL to viewport
    void findBBox(void);
    Eigen::AlignedBox<float,3> getBBox(void) const;

private:
    Eigen::Matrix<float, -1, 3, Eigen::RowMajor> m_points;
    QVector3D m_color;

    Eigen::AlignedBox<float, 3> m_bbox;

    QOpenGLVertexArrayObject m_vao;
    QOpenGLBuffer m_pbo; // point buffer
    QOpenGLBuffer m_nbo; // normal buffer
    QOpenGLBuffer m_cbo; // texture buffer
};

#endif
