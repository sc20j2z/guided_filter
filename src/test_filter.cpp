#include "Pcl.h"
#include "kdtree.h"

int main(int argc, char *argv[])
{
    bool use_knn = true;
    int knn = 10; // k-nearest-neighbors.
    double range = 0.1; // knn range search radius.
    float epsilon = 0.01;  // control filtering

    Pcl noisy_pcl;
    noisy_pcl.readFile("../data/plane_noisy.xyz"); // bun_zipper.xyz

    auto const & noisy_points = noisy_pcl.getPoints();
    Eigen::Matrix<float, -1, 3, Eigen::RowMajor> filtered_points = noisy_points;

    KdTree noisy_tree;
    noisy_tree.create(noisy_points);

    for(Eigen::Index i=0; i<noisy_points.rows(); ++i)
    {
        // 1: Calculate the neighbor hood {p_ij} of p_i
        std::vector<int> nbrs;
        if (use_knn) nbrs = noisy_tree.knnSearch(noisy_points.row(i), knn);
        else           nbrs = noisy_tree.rangeSearch(noisy_points.row(i), range);

        // 2: Compute the number of neighbors K = | {p_ij} |
        std::size_t K = nbrs.size();

        // 3. Center of neighbors
        Eigen::RowVector3f center = noisy_points(nbrs, Eigen::placeholders::all).colwise().mean();

        // 4. coefficients a_i
        float tmp = 0.0f;
        for(int nbr: nbrs)
            tmp += noisy_points.row(nbr).squaredNorm();
        float numerator = tmp / K - center.squaredNorm();
        float ai = numerator / (numerator + epsilon);

        // 5: coefficients bi
        Eigen::RowVector3f bi = center - ai * center;

        // 6:filtered points
        filtered_points.row(i) = ai * noisy_points.row(i) + bi;
    }

    noisy_pcl.setPoints(filtered_points);
    noisy_pcl.writeFile("../data/plane_filter.xyz");

    return 0;
}
