#ifndef PCLWIDGET_H
#define PCLWIDGET_H

#include <cmath>
#include <iostream>
#include <fstream>
#include <vector>

#include <QOpenGLFunctions>
#include <QOpenGLExtraFunctions> // OpenGL ES 3.0, 3.1 and 3.2
#include <QOpenGLShaderProgram>
#include <QOpenGLVertexArrayObject>
#include <QOpenGLBuffer>
#include <QOpenGLWidget>
#include <QKeyEvent>
#include <QTimer>
#include <QtMath>
#include <QFileDialog>

#include "Pcl.h"

class PclWidget : public QOpenGLWidget, protected QOpenGLFunctions
{
    // avoid QT looking for slots in base class.
    Q_OBJECT

public:
    explicit PclWidget(QWidget *parent = nullptr);

    ~PclWidget();

protected:
	void initializeGL() override;
	void paintGL() override;
	void resizeGL(int w, int h) override;

	void wheelEvent(QWheelEvent* event) override;
	void keyPressEvent(QKeyEvent *event) override;
	void mousePressEvent(QMouseEvent *event) override;
	void mouseReleaseEvent(QMouseEvent *event) override;
	void mouseMoveEvent(QMouseEvent *event) override;

    void filter();

    // use different
    void measureByChamfer();

signals:
    void noisyErrorChanged(QString text);
    void filteredErrorChanged(QString text);

public slots:
    void loadNoisyPCL();
    void loadGroundtruthPCL();
    void saveFilteredPCL();

    void changeRadius(double radius);
    void toggleNoisyPcl(bool checked);
    void toggleFilteredPcl(bool checked);
    void toggleGroundtruthPcl(bool checked);

    void toggleKnnSearch(bool checked);
    void changeKnnSearch(int k);
    void changeRangeSearch(double range);

    void changeEpsilon(double epsilon);

private:
	// use timer to animate
    std::unique_ptr<QTimer> m_timer;

    float m_fps = 24.f;

    QVector3D m_eye = {0.0, 0.0, 5.0};
    QVector3D m_center = {0.0, 0.0, 0.0};
    QVector3D m_up = {0.0, 1.0, 0.0};

    double m_radius; // point size

    // holding left mouse button to rotate object
    QPoint m_last_click;
    bool m_is_tracking;

    /*--------------- Shaders ---------------*/
    std::unique_ptr<QOpenGLShaderProgram> m_color_shader; // apply uniform color to a geometry.

    /*--------------- Transforms ---------------*/
    QMatrix4x4 m_projection;
    QMatrix4x4 m_view;
    QMatrix4x4 m_model;
    QMatrix4x4 m_model0; // initial transform before applying rotation.


    std::unique_ptr<Pcl> m_noisy_pcl;
    std::unique_ptr<Pcl> m_filtered_pcl;
    std::unique_ptr<Pcl> m_groundtruth_pcl;

    bool m_show_noisy_pcl = true;
    bool m_show_filtered_pcl = true;
    bool m_show_groundtruth_pcl = true;

    /*--------------- Filtering ---------------*/
    bool m_use_knn = true;  // use knn or range search.
    int m_knn = 10; // k-nearest-neighbors.
    double m_range = 0.1; // knn range search radius.
    float m_epsilon = 0.01;  // control filtering
};

#endif  /* PCLWIDGET_H */
