QT       += core gui opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
greaterThan(QT_MAJOR_VERSION, 5): QT += openglwidgets

CONFIG += c++17

SOURCES += \
        test_filter.cpp \
        Pcl.cpp \
        kdtree.cpp
		
HEADERS += \
        Pcl.h \
        kdtree.h
		
INCLUDEPATH += ../external/eigen

# https://doc.qt.io/qt-5/qmake-variable-reference.html#libs
win32:LIBS +=-lOpenGL32 -lglu32
