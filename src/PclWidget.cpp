#include "PclWidget.h"

PclWidget::PclWidget(QWidget * parent /* = nullptr */) : QOpenGLWidget(parent)
{
	// https://stackoverflow.com/questions/16499563/using-qts-qtimer-function-to-make-animations-in-opengl
	m_timer = std::make_unique<QTimer>();
	connect(m_timer.get(), SIGNAL(timeout()), SLOT(update()));
	m_timer->start(1000. / 30); // 30 milliseconds.

	//https://bugreports.qt.io/browse/QTBUG-42389
	this->setMouseTracking(true);
	this->setFocusPolicy(Qt::ClickFocus);
}

PclWidget::~PclWidget()
{
	makeCurrent();
}

void PclWidget::initializeGL()
{
	// initial OpenGL functions before use.
	initializeOpenGLFunctions();

	glClearColor(.8f, .8f, .83f, 1.0f);

	//glEnable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);
    //glEnable(GL_LIGHTING);

    m_color_shader = std::make_unique<QOpenGLShaderProgram>(this);
    m_color_shader->addShaderFromSourceFile(QOpenGLShader::Vertex, "./shaders/uniform_colours.vs");
    m_color_shader->addShaderFromSourceFile(QOpenGLShader::Fragment, "./shaders/uniform_colours.fs");
    m_color_shader->link();
    qDebug() << m_color_shader->log() << "\n";

    m_noisy_pcl = std::make_unique<Pcl>();
    m_filtered_pcl = std::make_unique<Pcl>();
    m_groundtruth_pcl = std::make_unique<Pcl>();

    m_noisy_pcl->setColor(QVector3D(0.0f, 1.0f, 0.0f));
    m_filtered_pcl->setColor(QVector3D(0.0f, 0.0f, 1.0f));
    m_groundtruth_pcl->setColor(QVector3D(1.0f, 0.0f, 0.0f));
}

void PclWidget::paintGL()
{
	// GL_DEPTH_BUFFER_BIT
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	//glClearColor(1.f, 0.5f, 0.5f, 1.f);
    glClearColor(0.8f, 0.8f, 0.8f, 1.f);

    glPointSize(m_radius);
    //qDebug() << "radius: " << m_radius;

    m_color_shader->bind();
    m_color_shader->setUniformValue("view", m_view);
    m_color_shader->setUniformValue("projection", m_projection);
    m_color_shader->setUniformValue("model", m_model);
    //qDebug() << "projection: " << m_projection << "\n";
    //qDebug() << "view: " << m_view << "\n";
    //qDebug() << "model: " << m_model << "\n";
    if (m_show_noisy_pcl) m_noisy_pcl->draw(m_color_shader.get());
    if (m_show_filtered_pcl) m_filtered_pcl->draw(m_color_shader.get());
    if (m_show_groundtruth_pcl) m_groundtruth_pcl->draw(m_color_shader.get());

    m_color_shader->release();
}

void PclWidget::resizeGL(int w, int h)
{
	glViewport(0, 0, w, h);
	update();

    // fixed camera for each subject
    m_view.setToIdentity();
    m_view.lookAt(m_eye, m_center, m_up);

    // must reset the matrix, otherwise will accumulate the results!!
    m_projection.setToIdentity();
    //m_projection.ortho(-1, 1, -1, 1, 0.1, 10000.);
    m_projection.perspective(45.0f, 1.0f * this->width() / this->height(), 0.1f, 10000.f);
}

void PclWidget::wheelEvent(QWheelEvent * event)
{
    const int degrees = event->delta() / 8;
    //qDebug() << degrees;

    m_eye.setZ(m_eye.z() - degrees / 15.0f);
    m_view.setToIdentity();
    m_view.lookAt(m_eye, m_center, m_up);
}

void PclWidget::keyPressEvent(QKeyEvent * event)
{
	switch (event->key())
	{
    case Qt::Key_1:
        break;

    case Qt::Key_E:
        break;
	}

}

void PclWidget::mousePressEvent(QMouseEvent * event)
{
	if (event->button() == Qt::LeftButton)
	{
        m_is_tracking = true;
        m_last_click = event->pos();
	}
}

void PclWidget::mouseReleaseEvent(QMouseEvent * event)
{
    if (event->button() == Qt::LeftButton)
    {
        m_is_tracking = false;
        m_model0 = m_model;
    }
}

QVector3D projectToSphere(QPoint pt, int width, int height)
{
    float x = 2.0f * pt.x() / width  - 1.f;
    float y = 1.0f - 2.0f * pt.y() / height;
    float d = qSqrt(x*x + y*y);
    float z = qCos(M_PI / 2.0f * d);

    float norm = qSqrt(x*x+y*y+z*z);
    return QVector3D(x/norm, y/norm, z/norm);
}

void PclWidget::mouseMoveEvent(QMouseEvent * event)
{
     //qDebug() << event->pos();
     //qDebug() << this->width() << " " << this->height();
	// move camera: holding left button and dragging
	if (event->buttons() & Qt::LeftButton)
	{
        if (m_is_tracking)
        {
            QPoint curr_pt = event->pos();
            QVector3D from = projectToSphere(m_last_click, this->width(), this->height());
            QVector3D to = projectToSphere(curr_pt, this->width(), this->height());
            QQuaternion rotation = QQuaternion::rotationTo(from, to);
            QMatrix4x4 T;
            T.rotate(rotation);
            m_model = T * m_model0;

            //qDebug() << "from: " << from << "\n";
            //qDebug() << "to: " << to << "\n";
            //qDebug() << m_model << "\n";
        }

    }
}

void PclWidget::filter()
{
    if (m_noisy_pcl->isEmpty())
        return;

    auto const & noisy_points = m_noisy_pcl->getPoints();
    Eigen::Matrix<float, -1, 3, Eigen::RowMajor> filtered_points = noisy_points;

    KdTree noisy_tree;
    noisy_tree.create(noisy_points);

    for(Eigen::Index i=0; i<noisy_points.rows(); ++i)
    {
        // 1: Calculate the neighbor hood {p_ij} of p_i
        std::vector<int> nbrs;
        if (m_use_knn) nbrs = noisy_tree.knnSearch(noisy_points.row(i), m_knn);
        else           nbrs = noisy_tree.rangeSearch(noisy_points.row(i), m_range);

        // 2: Compute the number of neighbors K = | {p_ij} |
        std::size_t K = nbrs.size();

        // 3. Center of neighbors
        Eigen::RowVector3f center = noisy_points(nbrs, Eigen::placeholders::all).colwise().mean();

        // 4. coefficients a_i
        float tmp = 0.0f;
        for(int nbr: nbrs)
            tmp += noisy_points.row(nbr).squaredNorm();
        float numerator = tmp / K - center.squaredNorm();
        float ai = numerator / (numerator + m_epsilon);

        // 5: coefficients bi
        Eigen::RowVector3f bi = center - ai * center;

        // 6:filtered points
        filtered_points.row(i) = ai * noisy_points.row(i) + bi;
    }

    m_filtered_pcl->setPoints(filtered_points);
    m_filtered_pcl->setupVAO();
}

void PclWidget::measureByChamfer()
{
    if (m_noisy_pcl->isEmpty() || m_filtered_pcl->isEmpty() || m_groundtruth_pcl->isEmpty())
        return;

    auto const & noisy_points = m_noisy_pcl->getPoints();
    auto const & filtered_points = m_filtered_pcl->getPoints();
    auto const & groundtruth_points = m_groundtruth_pcl->getPoints();

    KdTree noisy_tree;
    noisy_tree.create(noisy_points);

    KdTree filtered_tree;
    filtered_tree.create(filtered_points);

    KdTree groundtruth_tree;
    groundtruth_tree.create(groundtruth_points);

    // chamfer error of noisy PCL and groundtruth PCL
    float noisy_chamfer = 0.0;
    for(Eigen::Index i=0; i<noisy_points.rows(); ++i)
    {
        // find nearest neighbour in other PCL
        std::vector<int> noisy_nbrs = noisy_tree.knnSearch(groundtruth_points.row(i), 1);
        float groundtruth_error = (groundtruth_points.row(i) - noisy_points.row(noisy_nbrs.front())).norm();

        std::vector<int> groundtruth_nbrs = groundtruth_tree.knnSearch(noisy_points.row(i), 1);
        float noisy_error = (noisy_points.row(i) - groundtruth_points.row(groundtruth_nbrs.front())).norm();

        noisy_chamfer += (groundtruth_error + noisy_error) / groundtruth_points.rows();
    }
    QString noisy_chamfer_text = "noisy: " + QString::number(noisy_chamfer);
    emit noisyErrorChanged(noisy_chamfer_text);

    // chamfer error of filtered PCL and groundtruth PCL
    float filtered_chamfer = 0.0;
    for(Eigen::Index i=0; i<noisy_points.rows(); ++i)
    {
        // find nearest neighbour in other PCL
        std::vector<int> filtered_nbrs = filtered_tree.knnSearch(groundtruth_points.row(i), 1);
        float groundtruth_error = (groundtruth_points.row(i) - filtered_points.row(filtered_nbrs.front())).norm();

        std::vector<int> groundtruth_nbrs = groundtruth_tree.knnSearch(filtered_points.row(i), 1);
        float noisy_error = (filtered_points.row(i) - groundtruth_points.row(groundtruth_nbrs.front())).norm();

        filtered_chamfer += (groundtruth_error + noisy_error) / groundtruth_points.rows();
    }
    QString filtered_chamfer_text = "filtered: " + QString::number(filtered_chamfer);
    emit filteredErrorChanged(filtered_chamfer_text);
}

void PclWidget::loadNoisyPCL()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open"), "", tr("XYZ (*.xyz)"));
    if (fileName.isEmpty())
        return;

    m_noisy_pcl->readFile(fileName.toStdString());
    m_noisy_pcl->setupVAO();

    auto bbox = m_noisy_pcl->getBBox();

    auto center = bbox.center();
    auto spans = bbox.sizes();
    float max_span = spans.maxCoeff();
    QMatrix4x4 scaling; scaling.scale(1.f / max_span, 1.f/max_span, 1.f / max_span);
    QMatrix4x4 translation; translation.translate(-center.x(), -center.y(),-center.z());
    m_model0 = scaling * translation;
    m_model = m_model0;

    this->filter();
    this->measureByChamfer();
}

void PclWidget::loadGroundtruthPCL()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open"), "", tr("XYZ (*.xyz)"));
    if (fileName.isEmpty())
        return;

    m_groundtruth_pcl->readFile(fileName.toStdString());
    m_groundtruth_pcl->setupVAO();
    this->measureByChamfer();
}

void PclWidget::saveFilteredPCL()
{
    QString fileName = QFileDialog::getSaveFileName(this, tr("Save"), "", tr("XYZ (*.xyz)"));
    if (fileName.isEmpty())
        return;

    m_filtered_pcl->writeFile(fileName.toStdString());
}

void PclWidget::changeRadius(double radius)
{
    m_radius = radius;
}

void PclWidget::toggleNoisyPcl(bool checked)
{
    m_show_noisy_pcl = checked;
}

void PclWidget::toggleFilteredPcl(bool checked)
{
    m_show_filtered_pcl = checked;
}

void PclWidget::toggleGroundtruthPcl(bool checked)
{
    m_show_groundtruth_pcl = checked;
}

void PclWidget::toggleKnnSearch(bool checked)
{
    m_use_knn = checked;
    this->filter();
    this->measureByChamfer();
}

void PclWidget::changeKnnSearch(int k)
{
    m_knn = k;
    this->filter();
    this->measureByChamfer();
}

void PclWidget::changeRangeSearch(double range)
{
    m_range = range;
    this->filter();
    this->measureByChamfer();
}

void PclWidget::changeEpsilon(double epsilon)
{
    m_epsilon = epsilon;
    this->filter();
    this->measureByChamfer();
}

