#include "kdtree.h"

KdNode::KdNode() : parent(-1), left(-1), right(-1)
{
}

KdNode::KdNode(int parent_) : parent(parent_), left(-1), right(-1)
{
}

bool KdNode::isLeaf(void)
{
    return left == -1 && right == -1;
}

KdTree::KdTree()
{}

KdTree::~KdTree()
{
}

void KdTree::create(const Eigen::Matrix<float, -1, 3, Eigen::RowMajor> & points, int leaf_size)
{
    m_points = points;

    // n + (n/2) + (n/4) + ...= 2n-1
    // https://math.stackexchange.com/a/401976
    int num_est_leaf_nodes = m_points.rows() / leaf_size;
    m_nodes.reserve(2 * num_est_leaf_nodes);

    // the root has everything.
    auto &root_node = m_nodes.emplace_back();
    auto &root_element_ids = root_node.ids;
    root_element_ids.resize(m_points.rows());
    std::iota(root_element_ids.begin(), root_element_ids.end(), 0);
    root_node.bbox = Eigen::AlignedBox<float, 3>(m_points.colwise().minCoeff(), m_points.colwise().maxCoeff());

    // A FIFO queue to store which nodes need to be partitioned.
    for (std::queue<int> list({ 0 }); !list.empty(); list.pop())
    {
        int node_idx = list.front();
        auto & node = m_nodes[node_idx];
        auto & element_ids = node.ids;
        auto & bbox = node.bbox;

        // split if has too many elements.
        if (element_ids.size() > leaf_size)
        {
            // prepare two child nodes.
            std::size_t left_node_idx = m_nodes.size();
            KdNode left_child(node_idx);
            auto & left_bbox = left_child.bbox;
            auto & left_element_ids = left_child.ids;

            std::size_t right_node_idx = m_nodes.size() + 1;
            KdNode right_child(node_idx);
            auto & right_bbox = right_child.bbox;
            auto & right_element_ids = right_child.ids;

            // find the dimension with largest span
            int dof;
            bbox.sizes().maxCoeff(&dof);

            // sort points in selected 3ension
            std::size_t median = element_ids.size() / 2;
            std::nth_element(element_ids.begin(), element_ids.begin() + median, element_ids.end(), [&points, &dof](int i, int j) { return points(i, dof) < points(j, dof); });

            // update child elerments.
            left_element_ids.assign(element_ids.begin(), element_ids.begin() + median);
            left_bbox = Eigen::AlignedBox<float, 3>(m_points(left_element_ids, Eigen::placeholders::all).colwise().minCoeff(), m_points(left_element_ids, Eigen::placeholders::all).colwise().maxCoeff());

            right_element_ids.assign(element_ids.begin() + median, element_ids.end());
            right_bbox = Eigen::AlignedBox<float, 3>(m_points(right_element_ids, Eigen::placeholders::all).colwise().minCoeff(), m_points(right_element_ids, Eigen::placeholders::all).colwise().maxCoeff());

            // update parent node
            node.left = left_node_idx;
            node.right = right_node_idx;
            element_ids.clear();

            // add childrens(the resize will invalid the reference of element_ids...)
            m_nodes.emplace_back(std::move(left_child));
            m_nodes.emplace_back(std::move(right_child));

            // add child nodes to the list.
            list.push(left_node_idx);
            list.push(right_node_idx);
        }
    }
}

std::vector<int> KdTree::knnSearch(Eigen::Matrix<float, 1, 3> query_point, int k /* = 1 */)
{
    // allocate space.
    std::vector<int> indices(k);
    Eigen::Matrix<float, -1, 1> distances = Eigen::Matrix<float, -1, 1>::Constant(k, 1, std::numeric_limits<float>::max());

    // A FILO stack (depth first) to store which nodes to be checked next.
    for (std::vector<int> list{ 0 }; !list.empty(); )
    {
        int node_idx = list.back();
        list.pop_back();

        auto& node = m_nodes[node_idx];
        auto& bbox = node.bbox;

        // the distance between the point p and the box, and zero if p is inside the box.
        // https://eigen.tuxfamily.org/dox/classEigen_1_1AlignedBox.html#ad30a56f5c452e9c9f05f31df8de11281
        auto distance = bbox.exteriorDistance(query_point);

        // find currently maximum distance and replace it.
        int dof;
        auto cur_max_distance = distances.maxCoeff(&dof);

        // if all points of the bbox is too far away, discard.
        if (distance > cur_max_distance)
            continue;

        // add childs to testing list.
        if (!node.isLeaf())
        {
            if (node.left != -1) list.push_back(node.left);
            if (node.right != -1) list.push_back(node.right);
            continue;
        }

        for (int element_id : node.ids)
        {
            auto distance = (m_points.row(element_id) - query_point).norm();

            int dof;
            auto cur_max_distance = distances.maxCoeff(&dof);

            // update results.
            if (distance < cur_max_distance)
            {
                distances[dof] = distance;
                indices[dof] = element_id;
            }
        }
    }

    return indices;
}

std::vector<int> KdTree::rangeSearch(Eigen::Matrix<float, 1, 3> query_point, float range)
{
    // allocate space.
    std::vector<int> indices;

    // A FILO stack (depth first) to store which nodes to be checked next.
    for (std::vector<int> list{ 0 }; !list.empty(); )
    {

        int node_idx = list.back();
        list.pop_back();

        auto& node = m_nodes[node_idx];
        auto& bbox = node.bbox;

        // the distance between the point p and the box, and zero if p is inside the box.
        // https://eigen.tuxfamily.org/dox/classEigen_1_1AlignedBox.html#ad30a56f5c452e9c9f05f31df8de11281
        auto distance = bbox.exteriorDistance(query_point);

        // if all points of the bbox is too far away, discard.
        if (distance > range)
            continue;

        // add childs to testing list.
        if (!node.isLeaf())
        {
            if (node.left != -1) list.push_back(node.left);
            if (node.right != -1) list.push_back(node.right);
            continue;
        }

        for (int element_id : node.ids)
        {
            auto distance = (m_points.row(element_id) - query_point).norm();

            // update results.
            if (distance <= range)
                indices.push_back(element_id);
        }
    }

    return indices;
}
