#ifndef KDTREE_H
#define KDTREE_H

#include <vector>
#include <queue>
#include <numeric>
#include <iostream>
#include <sstream>

#include <Eigen/Core>
#include <Eigen/Geometry>

class KdNode
{
public:
    KdNode();
    KdNode(int parent);

    bool isLeaf(void);

public:
    int parent;
    int left, right;
    std::vector<int> ids;
    Eigen::AlignedBox<float, 3> bbox;
};


class KdTree
{
public:
    KdTree();
    ~KdTree();

    void create(const Eigen::Matrix<float, -1, 3, Eigen::RowMajor> & points, int leaf_size = 3);
    std::vector<int> knnSearch(Eigen::Matrix<float, 1, 3> query_point, int k = 1);
    std::vector<int> rangeSearch(Eigen::Matrix<float, 1, 3> query_point, float range);

private:
    Eigen::Matrix<float, -1, 3, Eigen::RowMajor> m_points;
    std::vector<KdNode> m_nodes;
};

#endif
