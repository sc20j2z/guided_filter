QT       += core gui opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
greaterThan(QT_MAJOR_VERSION, 5): QT += openglwidgets

CONFIG += c++17

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    main.cpp \
    mainwindow.cpp \
    PclWidget.cpp \
    Pcl.cpp \
    kdtree.cpp

HEADERS += \
    mainwindow.h \
    PclWidget.h \
    Pcl.h \
    kdtree.h

INCLUDEPATH += ../external/eigen

FORMS += \
    mainwindow.ui

# https://doc.qt.io/qt-5/qmake-variable-reference.html#libs
win32:LIBS +=-lOpenGL32 -lglu32

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
